﻿namespace App.Validator
{
    using App.Common;
    using Contracts;
    using System;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;

    public class XmlValidator : IXmlValidator
    {
        private const string XML_EXT = "xml";
        private const string XSD_RESRC = "App.Validator.XSD.class.xsd";

        public XmlValidator()
        {
        }

        public ErrorContainer ErrorContainer
        {
            get { return ErrorContainer.Instance; }
        }

        public XDocument Document { get; set; }

        public bool ValidateFile(string path)
        {
            ErrorContainer.Clear();
            string fileExt = Helpers.GetFileExtension(path);

            if (fileExt != XML_EXT)
            {
                ErrorContainer.AddError("The provided file is not XML");
                return false;
            }

            return PerformValidation(path);
        }

        private bool PerformValidation(string path)
        {
            var assembly = Assembly.GetExecutingAssembly();

            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add("", XmlReader.Create(assembly.GetManifestResourceStream(XSD_RESRC)));

            try
            {
                Document = XDocument.Load(path);
            }
            catch
            {
                ErrorContainer.AddError("An error occured while reading file");
                return false;
            }

            StringBuilder errorStack = new StringBuilder();
            bool isErrorFree = true;

            Document.Validate(schemas, (o, err) =>
            {
                errorStack.Append(err.Message).Append(Environment.NewLine).Append(Environment.NewLine);
                isErrorFree = false;
            });

            if (!isErrorFree)
            {
                ErrorContainer.AddError("The provided XML file is invalid", errorStack.ToString());
            }

            return isErrorFree;
        }
    }
}
