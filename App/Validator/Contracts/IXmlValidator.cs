﻿namespace App.Validator.Contracts
{
    using Common;

    public interface IXmlValidator
    {
        bool ValidateFile(string path);

        ErrorContainer ErrorContainer { get; }
    }
}
