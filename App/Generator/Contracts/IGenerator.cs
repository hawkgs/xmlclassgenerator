﻿namespace App.Generator.Contracts
{
    using Common;

    public interface IGenerator
    {
        void GenerateClass(string sourcePath, string outputPath, string lang);

        ErrorContainer ErrorContainer { get; }
    }
}
