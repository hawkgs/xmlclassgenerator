<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="method-args">(<xsl:if test="arguments"><xsl:for-each select="arguments/arg"><xsl:value-of select="@type" /><xsl:if test="@is-nullable">?</xsl:if>&#xA0;<xsl:value-of select="@name" /><xsl:if test="@default"> = <xsl:value-of select="@default" /></xsl:if><xsl:choose><xsl:when test="position() != last()">, </xsl:when></xsl:choose></xsl:for-each></xsl:if>)</xsl:template>
<xsl:template match="/class">namespace <xsl:value-of select="@namespace" />
{
    public <xsl:if test="@modifiers"><xsl:value-of select="@modifiers" />&#xA0;</xsl:if>class <xsl:value-of select="@name" /><xsl:if test="@inherits">: <xsl:value-of select="@inherits" /></xsl:if>
    {
        <!-- Fields -->
        <xsl:for-each select="field">
            <xsl:value-of select="@access" />&#xA0;<xsl:if test="@modifiers"><xsl:value-of select="@modifiers" />&#xA0;</xsl:if><xsl:value-of select="@type" />&#xA0;<xsl:value-of select="@name" /><xsl:if test="@value"> = <xsl:value-of select="@value" /></xsl:if>;
        </xsl:for-each>
        <!-- Constructors -->
        <xsl:for-each select="constructor">
        public <xsl:value-of select="../@name" /><xsl:call-template name="method-args" />&#xA0;
            <xsl:if test="base-args">: base(<xsl:if test="base-args/@arguments"><xsl:value-of select="base-args/@arguments"/></xsl:if>)</xsl:if>
        {<xsl:value-of select="code" />}

        </xsl:for-each>
        <!-- Properties -->
        <xsl:for-each select="property">
        <xsl:value-of select="@access" />&#xA0;<xsl:value-of select="@type" />&#xA0;<xsl:value-of select="@name" />
        {
            <xsl:if test="getter">
                <xsl:if test="getter/@access">
                    <xsl:value-of select="getter/@access" />&#xA0;</xsl:if>get
            {<xsl:value-of select="getter" />
            }
            </xsl:if>
            <xsl:if test="setter">
                <xsl:if test="setter/@access">
                    <xsl:value-of select="setter/@access" />&#xA0;</xsl:if>set
            {<xsl:value-of select="setter" />
            }
            </xsl:if>
        }

        </xsl:for-each>
        <!-- Methods -->
        <xsl:for-each select="method">
            <xsl:value-of select="@access" />&#xA0;<xsl:if test="@modifiers"><xsl:value-of select="@modifiers" />&#xA0;</xsl:if><xsl:value-of select="@type"/>&#xA0;<xsl:value-of select="@name" /><xsl:call-template name="method-args" />
        {<xsl:value-of select="code" />}

    </xsl:for-each>}
}
</xsl:template>
</xsl:stylesheet>
