<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="method-args">(<xsl:if test="arguments"><xsl:for-each select="arguments/arg"><xsl:value-of select="@name" /><xsl:if test="@default"> = <xsl:value-of select="@default" /></xsl:if><xsl:choose><xsl:when test="position() != last()">, </xsl:when></xsl:choose></xsl:for-each></xsl:if>)</xsl:template>
<xsl:template match="/class">
<xsl:value-of select="@namespace" /> = (function (<xsl:if test="@inherits"><xsl:value-of select="@inherits" /></xsl:if>) {
    "use strict";

    var <xsl:value-of select="@name" /> = class <xsl:if test="@inherits">extends <xsl:value-of select="@inherits" /></xsl:if> {
        <!-- Fields -->
        <xsl:for-each select="field">static <xsl:value-of select="@name" /><xsl:if test="@value"> = <xsl:value-of select="@value" /></xsl:if>;
        </xsl:for-each>
        <!-- Constructors -->
        <xsl:for-each select="constructor">
        constructor<xsl:call-template name="method-args" /> {
        <xsl:value-of select="code" />}

        </xsl:for-each>
        <!-- Properties -->
        <xsl:for-each select="property">
        <xsl:if test="getter">
        get <xsl:value-of select="@name" />() {<xsl:value-of select="getter" />
        }
        </xsl:if>
        <xsl:if test="setter">
        set <xsl:value-of select="@name" />(value) {<xsl:value-of select="setter" />
        }

        </xsl:if>
        </xsl:for-each>
        <!-- Methods -->
        <xsl:for-each select="method">
            <xsl:if test="@modifiers"><xsl:value-of select="@modifiers" />&#xA0;</xsl:if><xsl:value-of select="@name" /><xsl:call-template name="method-args" /> {<xsl:value-of select="code" />
        }

    </xsl:for-each>};

    return <xsl:value-of select="@name" />;
});
</xsl:template>
</xsl:stylesheet>
