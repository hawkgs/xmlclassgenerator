﻿namespace App.Generator
{
    using Common;
    using Contracts;
    using System.IO;
    using System.Xml;
    using System.Xml.Xsl;
    using System.Reflection;
    using System.Text.RegularExpressions;
    public class ClassGenerator : IGenerator
    {
        private const string CS_XSL = "App.Generator.XSLT.csharp.xsl";
        private const string JSE6_XSL = "App.Generator.XSLT.javascriptes6.xsl";

        public ClassGenerator() {}

        public ErrorContainer ErrorContainer
        {
            get { return ErrorContainer.Instance; }
        }

        public void GenerateClass(string sourcePath, string outputPath, string lang)
        {
            ErrorContainer.Clear();

            var assembly = Assembly.GetExecutingAssembly();
            XslCompiledTransform xslt = new XslCompiledTransform();
            string xslFile = string.Empty;
            string ext = string.Empty;

            SetLanguage(ref xslFile, ref ext, lang);

            string output = string.Format("{0}\\{1}.{2}", outputPath, Helpers.GetFileName(sourcePath), ext);

            xslt.Load(XmlReader.Create(assembly.GetManifestResourceStream(xslFile)));
            xslt.Transform(sourcePath, output);

            ClearXmlTag(output);
        }

        private void SetLanguage(ref string xslFile, ref string ext, string lang)
        {
            switch (lang)
            {
                case "C#":
                    xslFile = CS_XSL;
                    ext = "cs";
                    break;

                case "JavaScript (ES6)":
                    xslFile = JSE6_XSL;
                    ext = "js";
                    break;
            }
        }

        // The method might be slow
        private void ClearXmlTag(string filePath)
        {
            string text = File.ReadAllText(filePath);
            Regex xmlTag = new Regex(@"<\?xml\s(.)+?>");

            text = xmlTag.Replace(text, string.Empty);

            File.WriteAllText(filePath, text);
        }
    }
}
