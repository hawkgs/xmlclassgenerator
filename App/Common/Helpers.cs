﻿namespace App.Common
{
    public static class Helpers
    {
        public static string GetFileExtension(string path)
        {
            string[] pathSplit = path.Split('.');

            return pathSplit[pathSplit.Length - 1];
        }

        public static string GetFileName(string path)
        {
            string ext = GetFileExtension(path);
            string[] pathSplit = path.Split('\\');
            string file = pathSplit[pathSplit.Length - 1];

            return file.Replace(string.Concat(".", ext), string.Empty);
        }
    }
}
