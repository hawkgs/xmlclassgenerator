﻿namespace App.Common
{
    using System.Collections.Generic;

    public class ErrorContainer
    {
        private static ErrorContainer instance;

        private IList<ErrorItem> errors;

        protected ErrorContainer()
        {
            errors = new List<ErrorItem>();
        }

        public static ErrorContainer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ErrorContainer();
                }

                return instance;
            }
        }

        public void AddError(string message, string description = null)
        {
            errors.Add(new ErrorItem(message, description));
        }

        public void Clear()
        {
            errors.Clear();
        }

        public ErrorItem ReturnLastError()
        {
            return errors[errors.Count - 1];
        }
    }
}
