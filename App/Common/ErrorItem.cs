﻿namespace App.Common
{
    public class ErrorItem
    {
        public ErrorItem(string message, string description)
        {
            Message = message;
            Description = description;
        }

        public string Message { get; set; }
        public string Description { get; set; }
    }
}
