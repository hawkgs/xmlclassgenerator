﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Media;
using forms = System.Windows.Forms;
using App.Validator.Contracts;
using App.Validator;
using App.Common;
using App.Generator;
using App.Generator.Contracts;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SolidColorBrush errorColor;
        private SolidColorBrush successColor;
        private IXmlValidator validator;
        private IGenerator generator;

        public MainWindow()
        {
            InitializeComponent();

            // Initialize validator and generator
            validator = new XmlValidator();
            generator = new ClassGenerator();

            // Initialize brushes
            errorColor = new SolidColorBrush(Color.FromRgb(239, 60, 60));
            successColor = new SolidColorBrush(Color.FromRgb(54, 197, 88));
        }

        private ErrorContainer Errors
        {
            get { return ErrorContainer.Instance; }
        }

        private void ButtonOpenXmlFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == true)
            {
                sourceFile.Text = fileDialog.FileName;
            }
        }

        private void ButtonSelectOutput_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new forms.FolderBrowserDialog();

            dialog.ShowDialog();
            outputPath.Text = dialog.SelectedPath;
        }

        private void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (sourceFile.Text == string.Empty || 
                outputPath.Text == string.Empty ||
                outputLang.SelectedItem == null)
            {
                UpdateMessage("Some of the fields are empty", false);
                return;
            }

            bool validationResult = validator.ValidateFile(sourceFile.Text);
            if (!validationResult)
            {
                var lastError = Errors.ReturnLastError();

                UpdateMessage(lastError.Message, false);

                if (!string.IsNullOrEmpty(lastError.Description))
                {
                    MessageBox.Show(lastError.Description, lastError.Message);
                }

                return;
            }

            try
            {
                generator.GenerateClass(sourceFile.Text, outputPath.Text, outputLang.Text);
            }
            catch
            {
                UpdateMessage("An error occurred on class generating", false);
                return;
            }

            UpdateMessage("The operation has been successful", true);
        }

        private void UpdateMessage(string message, bool isSuccessful)
        {
            if (isSuccessful)
            {
                outputMsg.Background = successColor;
            }
            else
            {
                outputMsg.Background = errorColor;
            }

            outputMsg.Text = message;
            outputMsg.Visibility = Visibility.Visible;
        }
    }
}
